package java8.Interface;

import java.util.function.Function;

/**
 * 如果一个函数接收一个函数作为参数，或者返回一个函数作为返回值，那么该函数就称为高阶函数。
 * Function就是其一
 *
 */
public class FunctionTest01 {


    /**
     * Function接口的compose方法接收多个Function，将第一个参数调用apply方法
     * andThen方法和上面的正好相反，看源码就知道了
     *
     * @param args
     */
    public static void main(String[] args) {
        FunctionTest01 test01 = new FunctionTest01();

        //12
        System.out.println(test01.compute(2, value -> value * 3, value -> value * value));

        //36
        System.out.println(test01.compute(2, value -> value * 3, value -> value * value));

    }


    /**
     * compose方法为先应用参数的apply，在这里function2为参数，再应用当前的apply
     * @param a
     * @param function1
     * @param function2
     * @return
     */
    public int compute(int a, Function<Integer, Integer> function1, Function<Integer, Integer> function2) {
        return function1.compose(function2).apply(a);
    }


    /**
     * andThen方法为先应用当前的apply，再应用参数的apply
     * @param a
     * @param function1
     * @param function2
     * @return
     */
    public int compute2(int a, Function<Integer, Integer> function1, Function<Integer, Integer> function2) {
        return function1.andThen(function2).apply(a);
    }
}
