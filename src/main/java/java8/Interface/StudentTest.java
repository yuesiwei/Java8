package java8.Interface;

import java.util.function.Supplier;

/**
 * Supplier 不接收任何类型，返回一个泛型
 */
public class StudentTest {

    public static void main(String[] args) {
        Supplier<Student> supplier = () -> new Student();
        System.out.println(supplier.get().getName());

        System.out.println("-----------");


        /**
         * Student的构造方法引用
         */
        Supplier<Student> supplier1 = Student::new;
        System.out.println(supplier1.get().getName());






    }

}
