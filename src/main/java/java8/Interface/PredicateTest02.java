package java8.Interface;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class PredicateTest02 {

    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9);
        PredicateTest02 test = new PredicateTest02();
        test.getFiler(list, a -> a > 5);
        System.out.println("-------------------");
        test.getFiler(list, a -> a % 2 == 0);
        System.out.println("-------------------");
        test.getFiler(list, a -> true);//打印出所有的，没有任何判断，要清除，这段拉姆达表达式的类型只是boolean的

    }

    /**
     * 接收的是行为，只看代码是看不出这个方法具体的作用的，是比较抽象，比较宏观的，条件是使用者在调用的时候传进来的
     *
     * @param list
     * @param predicate
     */
    public void getFiler(List<Integer> list, Predicate<Integer> predicate) {
        for (Integer integer : list) {
            if (predicate.test(integer)) {
                System.out.println(integer);
            }
        }
    }
}
