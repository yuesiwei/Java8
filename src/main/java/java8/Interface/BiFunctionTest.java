package java8.Interface;

import java.util.function.BiFunction;

/**
 * Function接收一个参数，返回一个结果，不过BiFunction接收两个参数，返回一个结果
 * <p>
 * BiFunction可以编写一个计算器，正好接收两个参数，返回一个参数
 */
public class BiFunctionTest {

    public static void main(String[] args) {
        BiFunctionTest test = new BiFunctionTest();

        // 定义 + - * /
        System.out.println(test.compute(1, 2, (value1, value2) -> value1 + value2));
        System.out.println(test.compute(1, 2, (value1, value2) -> value1 - value2));
        System.out.println(test.compute(1, 2, (value1, value2) -> value1 * value2));
        System.out.println(test.compute(1, 2, (value1, value2) -> value1 / value2));
    }

    /**
     * apply 这个动作在使用的时候是不知道的，这个是需要用户传递进来的
     *
     * @param a
     * @param b
     * @param biFunction
     * @return
     */
    public int compute(int a, int b, BiFunction<Integer, Integer, Integer> biFunction) {
        return biFunction.apply(a, b);
    }

}
