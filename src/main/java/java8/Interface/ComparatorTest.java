package java8.Interface;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Comparator接口
 * jdk1.2中就已经出现了
 */
public class ComparatorTest {

    public static void main(String[] args) {

        //普通排序,sort方法默认是升序的，不过sort还有另一个重载方法，参数是Comparator接口
        List<Integer> list = Arrays.asList(2, 5, 7, 8, 9, 1, 0);
        Collections.sort(list);
        System.out.println(list);

        //这两种方法都是一样的，只不过下面的更简便一些，都是倒序排序
        Collections.sort(list, (a,b)->b.compareTo(a));
        Collections.sort(list, Comparator.reverseOrder());


        System.out.println(list);
    }


}
