package java8.Interface;

import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;
import java.util.stream.Collectors;


public class PersonTest {


    public static void main(String[] args) {
        Person person1 = new Person("zhangsan", 20);
        Person person2 = new Person("lisi", 30);
        Person person3 = new Person("wangwu", 40);

        List<Person> list = Arrays.asList(person1, person2, person3);
        list = getPersonByUsername("lisi", list);


        list = getPersonAge(21, list);


        /**
         * 重点：传递行为
         */
        list = getPersonByAge(20, list, (age, personList) -> personList.stream().filter(person -> person.getAge() > age).collect(Collectors.toList()));
        list.forEach(a-> System.out.println(a.getAge()));
    }

    /**
     * Predicate练习
     */
    public static List<Person> getPersonByUsername(String username, List<Person> personList) {
        return personList.stream()
                .filter(a -> a.getUsername().equals(username))
                .collect(Collectors.toList());
    }

    /**
     * BiFunction练习
     */
    public static List<Person> getPersonAge(int age, List<Person> persons) {
        BiFunction<Integer, List<Person>, List<Person>> biFunction = (agePerson, personList) -> personList.stream().filter(person -> person.getAge() > agePerson).collect(Collectors.toList());
        return biFunction.apply(age, persons);
    }


    /**
     * 增强方法，接收两个参数，返回一个参数
     *
     * @param age
     * @param personList
     * @param biFunction
     * @return
     */
    public static List<Person> getPersonByAge(int age, List<Person> personList, BiFunction<Integer, List<Person>, List<Person>> biFunction) {
        return biFunction.apply(age, personList);
    }
}
