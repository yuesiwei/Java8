package java8.Interface;

import java.util.function.Supplier;

/**
 * Supplier函数式接口，不接收任何泛型，返回一个结果
 * 具体查看 StudentTest.java
 */
public class SupplierTest {


    public static void main(String[] args) {
        Supplier<String> supplier = () -> "Hello World";
        System.out.println(supplier.get());
    }


}
