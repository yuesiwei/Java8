package java8.Interface;

import java.util.function.Predicate;

/**
 * 这个接口在流的使用中会被大量的应用，filter中使用的就是这个接口
 */
public class PredicateTest {

    public static void main(String[] args) {

        Predicate<Integer> predicate = p -> p > 5;

        //true
        System.out.println(predicate.test(6));

    }
}
