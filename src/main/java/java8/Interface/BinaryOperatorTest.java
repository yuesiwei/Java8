package java8.Interface;

import java.util.Comparator;
import java.util.function.BinaryOperator;

/**
 * BinaryOperator 继承了BiFunction函数式方法
 */
public class BinaryOperatorTest {

    public static void main(String[] args) {
        BinaryOperatorTest binaryOperatorTest = new BinaryOperatorTest();

        System.out.println(binaryOperatorTest.getCount(1, 2, (a, b) -> a + b));

        System.out.println(binaryOperatorTest.getMin("hello", "world", (a, b) -> a.length() - b.length()));
    }

    /**
     * 加法
     *
     * @param a
     * @param b
     * @param binaryOperator
     * @return
     */
    public int getCount(int a, int b, BinaryOperator<Integer> binaryOperator) {
        return binaryOperator.apply(a, b);
    }

    /**
     * 返回最小的一个数
     *
     * @param a
     * @param b
     * @param comparator
     * @return
     */
    public String getMin(String a, String b, Comparator<String> comparator) {
        return BinaryOperator.minBy(comparator).apply(a, b);
    }
}
