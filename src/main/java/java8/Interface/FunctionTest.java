package java8.Interface;

import java.util.function.Function;

/**
 * Function函数式接口传递的是行为，在Function以前，代码执行之前，行为代码就应该是存在的了
 * 但是在Function之后，行为是可以在代码执行时创建、并支持传递的了
 */
public class FunctionTest {


    public static void main(String[] args) {
        FunctionTest test = new FunctionTest();

        /**   a * 2 在这里就是行为   */
        System.out.println(test.compute(2, a -> a * 2));

        /**   把整数和字符串进行拼接，也是一种行为   */
        System.out.println(test.convert(2, a -> a + " Hello World"));

        //下面这也是一样的,只不过把行为摘出来了
        Function<Integer, Integer> function = value -> value + 1;
        System.out.println(test.compute(4, function));
    }

    /**
     * function接收的是行为，这个行为是传递者在使用的时候进行传递的
     *
     * @param a
     * @param function
     * @return
     */
    public int compute(int a, Function<Integer, Integer> function) {
        int result = function.apply(a);
        return result;
    }


    /**
     * function接收的参数为Integer 返回类型为String
     *
     * @param a
     * @param function
     * @return
     */
    public String convert(int a, Function<Integer, String> function) {
        return function.apply(a);
    }

}
